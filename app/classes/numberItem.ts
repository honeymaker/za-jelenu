export class NumberItem {
    hidden: boolean;
    times: boolean;
    timesValue: number;
    operation: string;
    value: number;
    calculatedValue: number;
    description: string;

    constructor(value: number, operation: string, times: boolean, timesValue: number, description: string) {
        this.value = value;
        this.operation = operation;
        this.times = times;
        this.description = description;

        if (this.times) {
            this.timesValue = timesValue;
        }

        this.calculatedValue = this.value * (this.timesValue ? this.timesValue : 1);
    }
}