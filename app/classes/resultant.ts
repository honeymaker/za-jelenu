import { NumberItem } from './numberItem';

export class Resultant {
    items: Array<NumberItem> = [];

    constructor() {
        console.log('CONST');
    } 

    get result() {
        return this.items.reduce((prevNum, currentNum, currentIndex, array) => {
            return eval(`${prevNum} ${currentNum.operation} ${currentNum.calculatedValue}`);
        }, 0);
    }

    setNewItem(newItem: NumberItem) {
        this.items.push(newItem);
        return this;
    }
}