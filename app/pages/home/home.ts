import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CounterPage } from '../counter/counter';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController) {
  }

  goToCounter() {
    this.navCtrl.push(CounterPage);
  }
}
