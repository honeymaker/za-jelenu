import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {FORM_DIRECTIVES} from '@angular/common';

import { NumberItem } from "../../classes/NumberItem";
import { Resultant } from '../../classes/resultant';

@Component({
  templateUrl: 'build/pages/counter/counter.html'
})
export class CounterPage {
  constructor(public navCtrl: NavController) {
  }

  timesEnabled = false;
  timesValue = 1;
  resultant= new Resultant();
  numberValue= 0;
  operation= false;
  numberDescription = "";

  saveNumber() {
      console.log(this.numberValue, this.operation);
  }

  calculateResult(operation: string) {
    let num = new NumberItem(this.numberValue, operation, this.timesEnabled, this.timesValue, this.numberDescription);
    if (num.calculatedValue !== 0) this.resultant.setNewItem(num);
    this.numberValue = null;
  }

  inputFocus() {
    if (this.numberValue === 0) {
      this.numberValue = null;
    }
  }

 timesFocus() {
   if (this.timesValue === 1) {
      this.timesValue = null;
    }
 }

 saveCalculations() {
   // Save somewhere
 }

}
